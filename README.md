# Matrix Regex Bot

This bot hosts a database of regex substitution patterns,
grouped per channel, and applies them to all incoming messages.

Feel free to invite @expressinator:rubdos.be .
Currently there are no admin commands.
