CREATE TABLE regexes (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    regex TEXT NOT NULL,
    replacement TEXT NOT NULL,
    creator TEXT NOT NULL,
    channel TEXT NOT NULL
);
