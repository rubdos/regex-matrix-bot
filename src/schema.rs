table! {
    regexes (id) {
        id -> Uuid,
        regex -> Text,
        replacement -> Text,
        creator -> Text,
        channel -> Text,
    }
}
