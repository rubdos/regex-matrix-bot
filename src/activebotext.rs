use matrix_bot_api::*;

pub trait ActiveBotExt {
    fn send_markdown_message(&self, msg: &str, room: &str, msgtype: MessageType);
}

impl ActiveBotExt for ActiveBot {
    fn send_markdown_message(&self, msg: &str, room: &str, msgtype: MessageType) {
        let html = comrak::markdown_to_html(msg, &comrak::ComrakOptions::default());
        self.send_html_message(msg, &html, room, msgtype);
    }
}
