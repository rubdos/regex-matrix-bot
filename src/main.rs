#[macro_use]
extern crate diesel;

use std::collections::HashMap;
use std::convert::TryFrom;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use matrix_bot_api::{
    handlers::{HandleResult, MessageHandler},
    ActiveBot, MatrixBot, Message, MessageType,
};
use regex::Regex;
use uuid::Uuid;

mod activebotext;
mod schema;

use activebotext::ActiveBotExt;

#[derive(Queryable)]
pub struct RegexSpec {
    id: Uuid,
    regex: String,
    replacement: String,
    creator: String,
    channel: String,
}

#[derive(Clone)]
pub struct CompiledRegex {
    id: Uuid,
    regex: Regex,
    replacement: String,
    creator: String,
    channel: String,
}

impl TryFrom<RegexSpec> for CompiledRegex {
    type Error = anyhow::Error;

    fn try_from(r: RegexSpec) -> Result<CompiledRegex, anyhow::Error> {
        Ok(CompiledRegex {
            id: r.id,
            regex: Regex::new(&r.regex)?,
            replacement: r.replacement,
            creator: r.creator,
            channel: r.channel,
        })
    }
}

struct RegexBotHandler {
    data: HashMap<String, Vec<CompiledRegex>>,
    pool: r2d2::Pool<ConnectionManager<PgConnection>>,
}

impl RegexBotHandler {
    fn load(pool: r2d2::Pool<ConnectionManager<PgConnection>>) -> Self {
        let mut rbh = RegexBotHandler {
            data: HashMap::new(),
            pool,
        };
        rbh.reload();
        rbh
    }

    fn reload(&mut self) {
        let data: Vec<RegexSpec> = {
            use schema::regexes::dsl::*;
            let conn = self.pool.get().unwrap();
            regexes.load(&*conn).unwrap()
        };

        let total = data.len();

        self.data.clear();
        for item in data {
            let v = self.data.entry(item.channel.clone()).or_insert(Vec::new());
            v.push(CompiledRegex::try_from(item).unwrap());
        }
        log::trace!("Loaded {} regexes for {} channels.", total, self.data.len());
    }
}

impl MessageHandler for RegexBotHandler {
    fn handle_message(&mut self, bot: &ActiveBot, msg: &Message) -> HandleResult {
        let channel = &msg.room;
        let regexes = if let Some(regexes) = self.data.get(channel) {
            regexes
        } else {
            return HandleResult::ContinueHandling;
        };

        for regex in regexes {
            if let Some(captures) = regex.regex.captures(&msg.body) {
                let mut replaced = regex.replacement.clone();
                for (i, replacement) in captures.iter().enumerate().skip(1) {
                    let replacement = if let Some(replacement) = replacement {
                        replacement
                    } else {
                        continue;
                    };
                    replaced = replaced.replace(&format!("\\{}", i), replacement.as_str());
                }

                bot.send_markdown_message(&replaced, channel, MessageType::RoomNotice);
            }
        }

        HandleResult::ContinueHandling
    }
}

fn main() {
    env_logger::init();

    let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name("regexbot.toml"))
        .unwrap();

    let user = settings.get_str("user").unwrap();
    let password = settings.get_str("password").unwrap();
    let homeserver_url = settings.get_str("homeserver_url").unwrap();

    let db = settings.get_str("db").unwrap();

    let manager = ConnectionManager::new(db);
    let pool = r2d2::Pool::new(manager).unwrap();

    let bot = MatrixBot::new(RegexBotHandler::load(pool));

    bot.run(&user, &password, &homeserver_url);
}
